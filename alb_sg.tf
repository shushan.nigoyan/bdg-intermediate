## Creating security group for Application load balancer.

resource "aws_security_group" "alb_sg" {
  name        = "alb_sg"
  description = "SG rules for ALB"

  ingress {
    description = "http"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "alb_sg"
  }
}
