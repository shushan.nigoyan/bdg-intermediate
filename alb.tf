###### Creating Application load balancer, target group and listener on port 80.

resource "aws_lb" "my_lb" {
  name               = "my-lb"
  internal           = false

}

resource "aws_lb_target_group" "my_target_group" {
  name     = "my-target-group"
  target_type = "instance"
  vpc_id   = aws_vpc.my_vpc.id

health_check {
  interval = 7
 }
}


  }
}
