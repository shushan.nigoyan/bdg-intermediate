# Creating bastion instance for accessing hosts in private subnets.

resource "aws_instance" "bastion" {
  ami = var.bastion_ami
  instance_type = "t2.micro"
  associate_public_ip_address = true
  
tags = {
    Name = "bastion"
  }
}
